// Raagav Prasanna 2036159
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] inventory = new Bicycle[4];
        inventory[0] = new Bicycle("Trek", 20, 50);
        inventory[1] = new Bicycle("Colnago", 25, 60);
        inventory[2] = new Bicycle("Connondale", 30, 55);
        inventory[3] = new Bicycle("Orbea", 22, 65);
        for(int i=0; i < inventory.length; i++) {
            System.out.println("Bicycle " +(i+1) +": " +inventory[i]);
        }
    }
}